---
layout: post
author: Filipe Saraiva
category: pencil-square
title: Inscrições para o curso de extensão "Python para Mineração de Dados"
---

O projeto de extensão __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) anuncia a oferta do curso de extensão "Python para Mineração de Dados".

Nesse curso serão explorados alguns conceitos de acesso, manipulação e visualização de dados.

Os pré-requisitos são lógica de programação e ser familiar com conceitos de orientação a objetos.

Serão disponibilizadas 30 vagas e as aulas acontecerão no Labcomp-02 do ICEN/UFPA, de 27 à 28 de Junho das 9:00 às 12:00. Aqueles que comparecerem a todas as aulas terão direito a um certificado de 6 horas.

__Atenção:__

`As inscrições são gratuitas mas aqueles que se inscreverem e, ou não comparecem ou faltarem a pelo menos uma das aulas, ficarão impossibilitados de se inscrever em cursos futuros.`

Vamos todos colaborar para que esse projeto tenha sucesso e aconteça por várias edições.

__Serviço:__

* __Atividade__: Curso "Python para Mineração de Dados
* __Data__: de 27 à 28 de junho de 2019
* __Horário__: das 9:00 às 12:00.
* __Local__: Labcomp-02 do ICEN-UFPA (Rua Augusto Corrêa n. 01 - Guamá)

__Formulário de Inscrições:__

<div class="embed-responsive embed-responsive-16by9">
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScR_U_tPxCIxLoqfIzzmZOk_IyBVU60Qn0yhZDTcgOTqqS_cA/viewform?embedded=true" width="640" height="1889" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
</div>
